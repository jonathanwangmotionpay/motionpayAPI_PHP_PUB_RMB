

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" " http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
  <head>
  <title>Motion Pay Sample Payment Page</title>    
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>  
	<meta charset="UTF-8"/>  
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link href="web.css" rel="stylesheet" />
	<link rel="icon" href="images/MotionpayIcon.gif" type="image/gif" sizes="16x16"/>
<script>
function imageSel(imageName) {
	var radioButtonEle = document.getElementById("bank_" + imageName);
	if(radioButtonEle) {
		radioButtonEle.checked = true;
	}
}
function validationForm(thisForm) {
	var radioButton1 = document.getElementById("bank_SCAN_ALIPAY");
	var radioButton2 = document.getElementById("bank_WX_SCANQRCODEPAY");
	var radioButton3 = document.getElementById("bank_H5_ALIPAY");
	var radioButton4 = document.getElementById("bank_H5_WECHAT");
	if (radioButton1.checked == false && radioButton2.checked == false 
			&& radioButton3.checked == false && radioButton4.checked == false) {
		alert("Please select one payment method.");
	}
	else {
		document.getElementById("payment").submit();
	}	
}
</script>	

  </head>
<body>

<!-- header -->
<div id="header">
  <div class="logo">
    <a href="#" class="logoImg logoPic" style="cursor: default;"></a>
  </div>
  <a href="http://motionpay.ca/" class="aProblem">Tech Support</a>
</div>
<!--header-->

<!--content-->
<div class="width1003" >
<div class="pay_infor"  >
    <div class="infor_box" style="height:30px;">
      <span >Sample Payment Page by <font style="font-size:15px;font-weight:bold;color:#2489c4;">Motion Pay</font></span>     
      <span style="float:right;"><a href="orderList.php"><font style="font-size:15px;font-weight:bold;color:#f60;">Order List</font></a></span> 
    </div>
</div>
  
  <form id="payment" action="payment.php" method="post" >
    <input type="hidden" id="currentURL" name="currentURL" value="" />
    <div class="bank_list accounts_pay" style="display:block;">
    
      <h6><label>Motion Pay Gateway</label></h6>
  		<label>Final Price: <font class="cOrange" style="font-size: 25px;">CNY ¥0.10</font></label> &nbsp;&nbsp;&nbsp;
 	  	<label>Other Amount: <input type="text" name="paymentAmount" value="0.10" size="5"/></label><br/> 
 	  	<b>(more than 10 cents is required)</b> <br/>
 	  	<br/><br/>
  	  <ul class="sel_list">
      	   <li >
	      	<label>
          	   <input type="radio" name="paymentType" id="bank_SCAN_ALIPAY" value="A" />
               <img src='images/alipay.png'/>
             </label>
           </li>
         
      	   <li >
	      	<label>
          	   <input type="radio" name="paymentType" id="bank_WX_SCANQRCODEPAY" value="W" />
               <img src='images/wechatpay.png'/>
             </label>
           </li>
<!-- 
      	   <li >
	      	<label>
          	   <input type="radio" name="paymentType" id="bank_H5_ALIPAY" value="H5_A" disabled/>
               <img src='images/alipayH5.png'/> (AliPay H5 CNY Price is not supported yet)
             </label>
           </li>
         
      	   <li >
	      	<label>
          	   <input type="radio" name="paymentType" id="bank_H5_WECHAT" value="H5_W" disabled/>
               <img src='images/wechatH5.png'/> (WeChat Pay H5 CNY Price is not supported yet)
             </label>
           </li>
 -->         
      </ul>
      <div class="confirm_pay">
        <a href="#" class="confirm_pay_btn" onclick="validationForm(this);"></a>
      </div>
    </div>
  </form>   
 
</div>

<script>
function getBaseURL() {
    var loc = window.location;
    var baseURL = loc.protocol + "//" + loc.hostname;
    if (typeof loc.port !== "undefined" && loc.port !== "") baseURL += ":" + loc.port;
    // strip leading /
    var pathname = loc.pathname;
    if (pathname.length > 0) pathname = pathname.replace("#","");
    if (pathname.length > 0) pathname = pathname.replace("index.php","");
    if (pathname.length > 0 && pathname.substr(0,1) === "/") pathname = pathname.substr(1, pathname.length - 1);
    var pathParts = pathname.split("/");
    if (pathParts.length > 0) {
        for (var i = 0; i < pathParts.length; i++) {
            if (pathParts[i] !== "") baseURL += "/" + pathParts[i];
        }
    }
    // alert("url is:" + baseURL);
    return baseURL;
}
function setCurrentURL() {	
	var currentURL = document.getElementById("currentURL");
	if(currentURL != null) {
		// alert("not null!");
		currentURL.value = getBaseURL()  + "/";
		// alert(currentURL.value);
	}
}
setCurrentURL();
</script>	
</body>

</html>


