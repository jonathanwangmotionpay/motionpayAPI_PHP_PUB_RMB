<?php
ini_set('date.timezone', 'Asia/Shanghai');
error_reporting(E_ALL);
// error_reporting(E_ERROR);
require_once "./lib/MotionPay.Config.php";
require_once "./lib/MotionPay.Data.php";
require_once "./lib/MotionPay.Api.php";
require_once './lib/Log.php';

// create log handler
// 初始化日志
$filename = MotionPayConfig::getMotionPayCallbackFilename();
// echo "filename is:" . $filename;
$logHandler = new CLogFileHandler($filename);
$log = Log::Init($logHandler, 15);

$log->INFO("got call back request!");
// read the json request
// 回调参数读取
$responseStr =  file_get_contents('php://input');
$log->INFO($responseStr);
$response = json_decode($responseStr, true);
$signServer = $response['sign'];

$input = new MotionPayDataBase();
$input->fromArray($responseStr, $log);
$input->setSign($log);
$signLocal = $input->getSign();

$log->INFO("signLocal:" . $signLocal);
$log->INFO("singServer:" . $signServer);
if ($signLocal == $signServer) {
    // the sign is correct
    // 验证成功
    
    // read the information in the request
    // 读取商户系统订单信息
    $mid = $response['mid'];
    $pay_channel = $response['pay_channel'];
    $pay_result = $response['pay_result'];
    $total_fee = $response['total_fee'];
    $transaction_id = $response['transaction_id'];
    $out_trade_no = $response['out_trade_no'];
    
    // put more business logic here to mark the order is paid.      
    // ——请根据您的业务逻辑来编写程序（以上代码仅作参考）——
    MotionPayConfig::logicForOrderPaid($out_trade_no);
    
    // send the success message back the server so no more call back request will be sent
    // 返回成功获得异步通知，服务器将不再发送后续通知
    $result = array('code' => '0', 'message' => 'success');
    echo json_encode($result);
} else {
    //sign is incorrect
    //验证失败
    echo "fail";
}
?>
