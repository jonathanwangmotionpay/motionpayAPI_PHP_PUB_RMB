



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" " http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
  <head>
  <title>Motion Pay Sample Payment Instruction</title>    
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>  
	<meta charset="UTF-8"/>  
	<link href="web.css" rel="stylesheet" />
	<link rel="icon" href="images/MotionpayIcon.gif" type="image/gif" sizes="16x16"/>
<script>
</script>	

</head>
<body>

<!-- header -->
<div id="header">
<div class="logo">
  <a href="#" class="logoImg logoPic" style="cursor: default;"></a>
</div>
<a href="http://motionpay.ca/" class="aProblem">Tech Support</a>
</div>
<!--header-->

<!--content-->
<div class="width1003" >
<div class="pay_infor"  >
  <div class="infor_box" style="height:30px;">
    <span>Sample Payment Project Instructions by <font style="font-size:15px;font-weight:bold;color:#2489c4;">Motion Pay</font></span>      
  </div>
</div>

<div class="pay_infor" style="text-align: left;">
<pre>
This is the PHP version for motionpay API.
Please download the Eclipse project file from this <a href="motionpayAPI_PHP_PUB_RMB.zip"><u>link</u></a>. 
Please download the develop tool <a href='https://www.eclipse.org/downloads/packages/eclipse-php-developers/oxygen3rc2'>Eclipse IDE for PHP Developers Oxygen Packages</a> 
The project can support PHP5, PHP6 and PHP7.
Please upload motionpayAPI_PHP_PUB_RMB folder in the zip file to your apache web folder.
The web folder we are using is: /var/www/html. You can use another folder if you want.
Please grant read and write permission to apache user for the log folder in the 
motionpayAPI_PHP_PUB_RMB in this package.
We need to generate log files in this folder.
You need to enable curl and gd lib in the PHP setting to do it. Let us know if you need any 
help about it.
For more information please check 
https://www.joomlashine.com/blog/how-to-enable-curl-in-php.html
or  http://php.net/manual/en/image.installation.php

1. You can try to visit the index.php fiel to test all the related function. There are 
   some other PHP testing files you can try in the same folder. 
   
2. All the configuration is in the lib/MotionPay.Config.php file. 
   It is the demo URL and demo account for testing for now. After you register and finish 
   the integration, you can replace it with the live URL and live account.
   
3. We are PHP 7 for testing. You can run it in PHP 5 and PHP 6 too. If you are using 
   a platform and need some specified help, please contact us.
   Visit index.php to select the payment method. We can support 3 types of payment now:
   1) PC WEB browser generate a QR code on the screen then use mobile APP AliPay to 
   scan it for the payment.
   2) PC WEB browser generate a QR code on the screen then use mobile APP Wechat Pay 
   to scan it for the payment.
   
4. If you have any question or need any help, please send an email to 
   jonathan.wang@motionpay.ca.
</pre> 

<br/> <br/>
<pre>
这是 PHP 版本的 motionpay API 开发包。
请点击该链接下载 API demo的 Eclipse 工程文件包： <a href="motionpayAPI_PHP_PUB_RMB.zip"><u>下载</u></a>. 
请点击该链接下载我们使用的开发工具 <a href='https://www.eclipse.org/downloads/packages/eclipse-php-developers/oxygen3rc2'>Eclipse IDE for PHP Developers Oxygen Packages</a>.
该程序可以支持 PHP5, PHP6 和 PHP7.
您可以把压缩包中  motionpayAPI_PHP_PUB_RMB 目录上载到 apache web server的工作
目录来运行测试。
我们所使用的 目录是 /var/www/html/。 您可以使用其他任何目录，
在motionpayAPI_PHP_PUB_RMB目录中的log目录，我们需要在这个目录中生成 日志文件。
请对该目录设置读写权限给 apache 用户。
程序中使用了 curl 和 gd 库函数。 您需要更改 php.ini 文件 来开启者连个函数库。
您可以在下面两个链接中找到关于如何修改这两个设置的更详细的信息:
https://www.joomlashine.com/blog/how-to-enable-curl-in-php.html
http://php.net/manual/en/image.installation.php
如果您需要帮助，请随时联系我们。

1. 你可以直接运行index.php来测试所有功能。在同一个目录下还有其他程序可以用来做测试。
   
2. 所以的配置信息都在lib/MotionPay.Config.php文件中。
          目前该文件中的信息是测试URL和测试账户。您完成集成工作并申请获得正式账户之后，
          可以替换掉该文件中的参数。
           
3. 我们使用的是 PHP7, 您也可以使用 PHP5或PHP6.如果您发现了什么问题，或是使用了
          其他电商平台需要额外的帮助，请联系我们。
          您可以访问 index.php文件来进行测试。我们目前支持三种集成方式：
   1) 在个人电脑的浏览器中生成一个二维码，然后使用支付宝进行扫码支付。
   2) 在个人电脑的浏览器中生成一个二维码，然后使用微信支付进行扫码支付。
   
4. 如果您有任何技术问题或是需要帮助，请发送邮件到 jonathan.wang@motionpay.ca 多谢！
</pre>
</div>
</div>
</body>
</html>