<?php
ini_set('date.timezone', 'Asia/Shanghai');
require_once "./lib/MotionPay.Config.php";
require_once "./lib/MotionPay.Data.php";
require_once "./lib/MotionPay.Api.php";
require_once './lib/Log.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" " http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
  <head>
  <title>Motion Pay Sample Order List Page</title>    
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>  
	<meta charset="UTF-8"/>  
	<link href="web.css" rel="stylesheet" />
	<link rel="icon" href="images/MotionpayIcon.gif" type="image/gif" sizes="16x16"/>
<script>
</script>	
<style>
table, td, th {
    text-align: left;
}
</style>
  </head>
<body>

<!-- header -->
<div id="header">
  <div class="logo">
    <a href="#" class="logoImg logoPic" style="cursor: default;"></a>
  </div>
  <a href="http://motionpay.ca/" class="aProblem">Tech Support</a>
</div>
<!--header-->

<!--content-->
<div class="width1003" >
<div class="pay_infor"  >
    <div class="infor_box" style="height:30px;">
      <span>Sample Order Refund Page by <font style="font-size:15px;font-weight:bold;color:#2489c4;">Motion Pay</font></span>      
      <span style="float:right;"><a href="orderList.php"><font style="font-size:15px;font-weight:bold;color:#f60;">Order List</font></a></span> 
    </div>
</div>
  
    <div class="bank_list accounts_pay" style="display:block;">
    
      <h6><label>Motion Pay Order Refund Result</label></h6>
  	  <ul class="sel_list">
<?php 
$out_trade_no = "";
$mid = "";
$refundAmount = "";
$totalAmount = "";

if(isset($_POST['out_trade_no'])) {
    $out_trade_no = $_POST['out_trade_no'];
}
if(isset($_POST['mid'])) {
    $mid = $_POST['mid'];
}
if(isset($_POST['refundAmount'])) {
    $refundAmount = $_POST['refundAmount'];
}
if(isset($_POST['totalAmount'])) {
    $totalAmount = $_POST['totalAmount'];
}

if(strlen($out_trade_no) > 0 && strlen($mid) > 0 && strlen($refundAmount) > 0 && strlen($totalAmount) > 0) {
    $message = "";
    $input = new MotionPayRefund();
    $input->setMerchantTypeByMerchantId($mid);
    $input->setOutTradeNo($out_trade_no);
    $input->setTotalFee($totalAmount*100);
    $input->setRefundAmount($refundAmount*100);
    $resultReturn = MotionPayApi::refund($input);
    if ($resultReturn['code'] == '0') {
        $result = $resultReturn['content'];
    }
    else {
        $message = $resultReturn['message'];
    }
    
    if(strlen($message) > 0) {
        echo "error:" . $message;
    }
    else {
        echo("<table>");
        echo("<tr><td>refund_fee: </td><td>¥" . $refundAmount . "</td></tr>");
        echo("<tr><td>total_fee: </td><td>¥" . $totalAmount . "</td></tr>");
        echo("<tr><td>pay_channel:</td><td>" . $result["pay_channel"] . "</td></tr>");
        echo("<tr><td>refund_status: </td><td>" . $result["refund_status"] . "</td></tr>");
        echo("</table>");
    }
}
else {
    echo "out_trade_no, mid, refundAmount or totalAmount is empty.\n";
}
?>
           
      </ul>
    </div>

 
</div>

</body>

</html>

