<?php
ini_set('date.timezone', 'Asia/Shanghai');
require_once "./lib/MotionPay.Config.php";
require_once "./lib/MotionPay.Data.php";
require_once "./lib/MotionPay.Api.php";
require_once './lib/Log.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" " http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
  <head>
  <title>Motion Pay Sample Order List Page</title>    
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>  
	<meta charset="UTF-8"/>  
	<link href="web.css" rel="stylesheet" />
	<link rel="icon" href="images/MotionpayIcon.gif" type="image/gif" sizes="16x16"/>
<script>
</script>	
<style>
table, td, th {
    text-align: left;
}
</style>
  </head>
<body>

<!-- header -->
<div id="header">
  <div class="logo">
    <a href="#" class="logoImg logoPic" style="cursor: default;"></a>
  </div>
  <a href="http://motionpay.ca/" class="aProblem">Tech Support</a>
</div>
<!--header-->

<!--content-->
<div class="width1003" >
<div class="pay_infor"  >
    <div class="infor_box" style="height:30px;">
      <span>Sample Order Detail Page by <font style="font-size:15px;font-weight:bold;color:#2489c4;">Motion Pay</font></span>      
  	  
      <span style="float:right;"><a href="orderList.php"><font style="font-size:15px;font-weight:bold;color:#f60;">Order List</font></a></span> 
    </div>
</div>
  
  <form id="payment" action="orderRefund.php" method="post" >
    <div class="bank_list accounts_pay" style="display:block;">
    
      <h6><label>Motion Pay Order Detail Sample Page</label></h6>
  	  <ul class="sel_list">
<?php 
$out_trade_no = "";
$mid = "";
if(isset($_GET['out_trade_no'])) {
    $out_trade_no = $_GET['out_trade_no'];
}
if(isset($_GET['mid'])) {
    $mid = $_GET['mid'];
}

if(strlen($out_trade_no) > 0 && strlen($mid) > 0) {
    $message = "";
    $input = new MotionPayOrderQuery();
    $input->setMerchantTypeByMerchantId($mid);
    $input->setOutTradeNo($out_trade_no);
    $resultReturn = MotionPayApi::orderQuery($input);
    if ($resultReturn['code'] == '0') {
        $result = $resultReturn['content'];
    }
    else {
        $message = $resultReturn['message'];
    }
    
    if(strlen($message) > 0) {
        echo "error:" . $message;
    }
    else {
        // var_dump($result);

        $total_fee = $result["total_fee"];
        $amountPaid = $total_fee * 1.0 / 100;
        $settlement_amount = $result["settlement_amount"];
        try {
            $settlement_amount =  $settlement_amount * 1.0 / 100;;
        }
        catch (Exception $e) {
        }
        echo ("<table>\n");
        echo ("<tr><td>total_fee: </td><td>¥" . $amountPaid . "</td></tr>\n");
        echo ("<tr><td>currency_type: </td><td>" . $result["currency_type"] . "</td></tr>\n");
        echo ("<tr><td>pay_channel:</td><td>" . $result["pay_channel"] . "</td></tr>\n");
        echo ("<tr><td>trade_status: </td><td>" . $result["trade_status"] . "</td></tr>\n");
        echo ("<tr><td>exchange_rate: </td><td>" . $result["exchange_rate"] . "</td></tr>\n");
        echo ("<tr><td>settlement_amount: </td><td>$" . $settlement_amount . "</td></tr>\n");
        echo ("<tr><td>user_identify: </td><td>" . $result["user_identify"] . "</td></tr>\n");
        echo ("<tr><td>Refund Amount:  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>\n");
        echo ("<td><input type='text' name='refundAmount' value='" . $amountPaid . "' size='5'/> &nbsp;\n");
        echo ("<input type='hidden' name='totalAmount' value='" . $amountPaid . "'>\n");
        echo ("<input type='hidden' name='out_trade_no' value='" . $out_trade_no . "'>\n");
        echo ("<input type='hidden' name='mid' value='" . $mid . "'>\n");
        echo ("<input type='submit' name='refund' value='Refund'></td></tr>\n");
        echo ("</table>\n");
        
    }
}
else {
    echo "out_trade_no or mid is empty.\n";
}
?>
           
      </ul>
    </div>
  </form>
 
</div>

</body>

</html>


