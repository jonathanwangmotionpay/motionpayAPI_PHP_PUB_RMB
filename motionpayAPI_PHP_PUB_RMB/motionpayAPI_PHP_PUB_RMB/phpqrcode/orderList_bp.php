<?php
ini_set('date.timezone', 'Asia/Shanghai');
require_once "./lib/MotionPay.Config.php";
require_once "./lib/MotionPay.Data.php";
require_once "./lib/MotionPay.Api.php";
require_once './lib/Log.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" " http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
  <head>
  <title>Motion Pay Sample Order List Page</title>    
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>  
	<meta charset="UTF-8"/>  
	<meta name="viewport" content="width=device-width, initial-scale=1"/>

	<link rel="icon" href="images/MotionpayIcon.gif" type="image/gif" sizes="16x16"/>
	
	
	<!-- BOOTSTRAP CSS and JS. Could be replaced by hosted BOOTSTRAP-4 after download ------> 
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
	
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	
	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
	
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
	<!-- BOOTSTRAP CSS and JS. Could be replaced by hosted BOOTSTRAP-4 after download ------>
	
	
  </head>


<body>
<div class="container">

<!-- header -->
<center>
<div id="header" class="w-50">
	  <div class="text-left">
	    <a href="#"><img src="images/Motionpay-Logo.gif"></img></a>	    
	  	<a href="http://motionpay.ca/"  class="float-right text-mute"><br/><small>Tech Support</small></a>
	  </div>	
	  
	  <hr class="table-bordered">
</div>
</center>
<!--header-->


<!--content-->
<center>
<div name="outerbox" class="w-50 table-bordered">
	
	<div name="toptable" style="background:#edffcd;">
			<br/>
			<span class="float-left">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				Sample Payment Page by 
				<span class="text-primary">
						Motion Pay</span>
			</span>
			
	  	<strong class="float-right">
	  		<a href="index_bp.php">
	  				<u><span class="text-warning">Payment Page</span></u></a>
	  		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  	</strong>
	  	<br/>
	  	</span>&nbsp;</span>
  </div> <!-- end div-toptabel --->
  
  <div name="infotable">
		<br/>
		<div class="w-75  text-left">
		    <h6>
		    	Motion Pay Order List
		    </h6>

    	    <ul class="list-group">
            	<?php
                    $filename = MotionPayConfig::getMotionPayCallbackFilename();
                    // echo "filename is:" . $filename;
                    
                    if(file_exists($filename) == false) {
                        echo "file doesn't exist:" . $filename . "\n";
                    }
                    else {
                        foreach(file($filename) as $line) {
                            if(strlen($line) > 0) {
                                $pos = strpos($line, "out_trade_no");
                                if($pos > 0) {
                                    $info = "[info]";
                                    $pos = strpos($line,$info);
                                    $jsonInlog = substr($line,$pos + strlen($info));
                                    $jsonStr = trim($jsonInlog);
                                    
                                    // echo "jsonStr:" . $jsonStr;
                                    $arrayResult = json_decode($jsonStr, true);
                                    $out_trade_no = $arrayResult['out_trade_no'];
                                    $pay_result = $arrayResult['pay_result'];
                                    $total_fee = $arrayResult['total_fee'];
                                    $mid = $arrayResult['mid'];
                                    
                                    if (strlen($out_trade_no) > 0) {
                                        echo "<li class='list-group-item'>\n";
                                        echo "OrderId: <a href='orderDetail_bp.php?out_trade_no=" . $out_trade_no . "&mid=" . $mid . "'>" . $out_trade_no . "</a>";
                                        echo "</li>\n";
                                    }
                                }
                            }
                        }
                    }
                ?>
    		</ul>
		<div>
		
</div> <!-- end div-infotable --->
	
</div> <!-- end div-outbox --->
</center>

</div> <!-- end div-container -->
</body>

</html>
