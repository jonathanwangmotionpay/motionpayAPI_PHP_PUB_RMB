<?php
require_once "MotionPay.Exception.php";
require_once "MotionPay.Config.php";
require_once "MotionPay.Data.php";
require_once 'Log.php';

/**
 * API Interface, includes all static functions we need in the API
 * Default timeout value is 15 seconds
 * @author Jonathan
 */
/**
 *
 * 接口访问类，包含所有MotionPay支付API列表的封装，类中方法为static方法，
 * 每个接口有默认超时时间（15s）
 * @author Jonathan
 *
 */
class MotionPayApi
{    
    
    private function __construct()
    {
    }
    
    /**
     * generate random numbers no more than 30 in length
     * @param int $length
     * @return $str generated random number
     */
    /**
     *
     * 产生随机字符串，不长于30位
     * @param int $length
     * @return $str 产生的随机字符串
     */
    public static function getNonceStr($length = 30)
    {
        $chars = "0123456789";
        $str = "";
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $str;
    }

    /**
     * Get QRCODE URL
     * 
     * @param MotionPayOrder $inputObj
     * @param int $timeOut
     * @throws MotionPayException
     * @return $result the array with values from the server
     */
    /**
     *
     * QR下单
     * @param MotionPayOrder $inputObj
     * @param int $timeOut
     * @throws MotionPayException
     * @return $result 成功时返回，其他抛异常
     */
    public static function qrOrder($inputObj, $timeOut = 15)
    {
        $url = MotionPayConfig::getURL(MotionPayConfig::PRE_PAY);
        $inputObj->setSign();
        $response = self::postJsonCurl($url, $inputObj, $timeOut);
        $result = MotionPayResults::init($response);
        return $result;
    }

    /**
     * Get H5 order URL
     *
     * @param MotionPayOrder $inputObj
     * @param int $timeOut
     * @throws MotionPayException
     * @return $result the array with values from the server
     */
    /**
     *
     * H5下单
     * @param MotionPayOrder $inputObj
     * @param int $timeOut
     * @throws MotionPayException
     * @return $result 成功时返回，其他抛异常
     */
    public static function H5Order($inputObj, $timeOut = 15)
    {
        $channel = $inputObj->getPayChannel();
        if(strlen($channel) > 1) {
            $channel = str_replace("H5_", "", $channel);
        }
        $inputObj->setPayChannel($channel);
        $inputObj->setMerchantType(MotionPayConfig::H5_MERCHANT);
        $url = MotionPayConfig::getURL(MotionPayConfig::H5_PAY);
        $inputObj->setSign();
        $response = self::postJsonCurl($url, $inputObj, $timeOut);
        $result = MotionPayResults::init($response);
        return $result;
    }


    /**
     * Query Order
     *
     * @param MotionPayOrder $inputObj
     * @param int $timeOut
     * @throws MotionPayException
     * @return $result the array with values from the server
     */
    /**
     *
     * 查询订单
     * @param MotionPayOrderQuery $inputObj
     * @param int $timeOut
     * @throws MotionPayException
     * @return $result 成功时返回，其他抛异常
     */
    public static function orderQuery($inputObj, $timeOut = 15)
    {
        $url = MotionPayConfig::getURL(MotionPayConfig::ORDER_QUERY);
        $inputObj->setSign();
        $response = self::postJsonCurl($url, $inputObj, $timeOut);
        $result = MotionPayResults::init($response);
        return $result;
    }
    
    /**
     * Refund an order
     *
     * @param MotionPayOrder $inputObj
     * @param int $timeOut
     * @throws MotionPayException
     * @return $result the array with values from the server
     */
    /**
     *
     * 申请退款
     * @param Object $inputObj
     * @param int $timeOut
     * @throws MotionPayException
     * @return $result 成功时返回，其他抛异常
     */
    public static function refund($inputObj, $timeOut = 15)
    {
        $url = MotionPayConfig::getURL(MotionPayConfig::ORDER_REVOKE);
        $inputObj->setSign();
        $response = self::postJsonCurl($url, $inputObj, $timeOut);
        $result = MotionPayResults::init($response);
        return $result;
    }

    
    /**
     * Send JSON request to the URL in parameter with POST method
     *
     * @param string $url
     * @param object $inputObj
     * @param int $second timeout value default as 30 seconds
     * @throws MotionPayException
     */
    /**
     * 以post方式提交json到对应的接口url
     *
     * @param string $url
     * @param object $inputObj
     * @param int $second url执行超时时间，默认30s
     * @throws MotionPayException
     */
    private static function postJsonCurl($url, $inputObj, $second = 30)
    {
        
        $logHandler = new CLogFileHandler(MotionPayConfig::getMotionPayLogFilename());
        $log = Log::Init($logHandler, 15);
        
        $jsonStr = $inputObj->toBodyParams();
        // echo "jsonStr is:" . $jsonStr;
        // echo "url is:" . $url;
        $log->INFO("json request string is:" . $jsonStr);
        
        // for testing.
        // $jsonStr = '{"goods_info":"Test_Product","mid":"100105100000011","out_trade_no":"201802048567163482469","pay_channel":"W","return_url":"https://demo.motionpay.org/motionpayAPI_JAVA_PUB/CallbackServlet","sign":"6F2EA42E7DFDB6526CF2223AEED47015E5BF67F5","spbill_create_ip":"192.168.1.1","terminal_no":"WebServer","total_fee":1}';
        
        $ch = curl_init();
        //set timeout
        //设置超时
        curl_setopt($ch, CURLOPT_TIMEOUT, $second);
        //set proxy info if we have
        //如果有配置代理这里就设置代理
        if (MotionPayConfig::CURL_PROXY_HOST != "0.0.0.0"
            && MotionPayConfig::CURL_PROXY_PORT != 0
            ) {
                curl_setopt($ch, CURLOPT_PROXY, MotionPayConfig::CURL_PROXY_HOST);
                curl_setopt($ch, CURLOPT_PROXYPORT, MotionPayConfig::CURL_PROXY_PORT);
        }
        
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); 
        //set header
        //设置header
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        
        curl_setopt($ch, CURLOPT_POST, 1);
        
        // echo "json request string is:" . $jsonStr;
        curl_setopt($ch, CURLOPT_POSTFIELDS,$jsonStr);
        
        //use curl to post to url
        //运行curl
        $data = curl_exec($ch);
        $log->INFO("json response string is:" . $data);

        // return values
        // 返回结果
        if ($data) {
            curl_close($ch);
            return $data;
        } else {
            $error = curl_errno($ch);
            curl_close($ch);
            throw new MotionPayException("curl出错，错误码:$error");
        }
    }    
}

